import React from 'react';
import { ScrollView, StyleSheet, Image, TouchableOpacity} from 'react-native';

const klik =(props) => {
    return(
        <ScrollView horizontal>
            <TouchableOpacity onPress={props.onButtonpress}>
                <Image source ={require('../assets/menyukai.jpg')} style ={styles.menyukai}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source ={require('../assets/komentar.png')} style= {{width : 20, height: 20, marginLeft:1, marginTop: 14 }}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source ={require('../assets/bagi.jpg')} style= {{width :25, height: 25, marginLeft : 10, marginTop: 13}}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source ={require ('../assets/menyimpan.png')} style ={{width: 30, height: 30, marginLeft : 270, marginTop : 13}}/>
            </TouchableOpacity>
        </ScrollView>

    );
};

const styles =StyleSheet.create({
    menyukai :{
        width :45,
        height : 40,
        marginLeft : 10,
        marginTop: 1 
    }
});

export default klik; 